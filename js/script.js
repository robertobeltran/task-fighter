var json = {
	"display name": "User",
	"level": 1,
	"progress": 22,
	"today": ["Complete This Task", "Fail This Task", "Delete This Task", "Add More Tasks"]
};
function loadPage(json) {
	loadElement("level", json["level"]);
	loadProgressBar()
	loadElement("display-name", json["display name"])
	loadElement("next-task", json["today"][0]);
	loadToday();
}
function loadProgressBar() {
	fill = document.getElementById("fill");
	fill.style.width = (json["progress"]) + "%";
	fill.innerHTML = json["progress"] + "%";
}

function spliceTask(task, adjustment) {
	json["today"].splice(parseInt(task.value), 1);
	adjustProgress(adjustment);
	loadToday();
}

function loadToday() {
	ol = document.getElementById("today");
	while (ol.firstChild) {
    ol.removeChild(ol.firstChild);
	}
	for (var i = 1; i < json["today"].length; i++) {
		var task = json["today"][i]; 
		var li = document.createElement("li");
		var complete = document.createElement("button");
		var fail = document.createElement("button");
		var del = document.createElement("button");

		complete.setAttribute("class", "complete-task btn-success-small fas fa-check");
		fail.setAttribute("class", "fail-task btn-danger-small fas fa-times");
		del.setAttribute("class", "delete-task btn-warning-small fas fa-trash");

    li.setAttribute("value", i);

		li.appendChild(complete);
		li.appendChild(fail);
		li.appendChild(del);
		li.innerHTML += " " + task;

		ol.appendChild(li);
	}
}
function loadElement(id, data) {
	document.getElementById(id).innerHTML = data;
}
function adjustProgress(amount) {
	json["progress"] += amount;
	loadProgressBar();
}
function shiftNext(progressAdjustment) {
	if (json["today"].length) {
		json["today"].shift();
		adjustProgress(progressAdjustment);
		loadToday();
		if (json["today"].length) {
			loadElement("next-task", json["today"][0]);
		}
		else {
			loadElement("next-task", "Done for today!");
		}
	}
}
function addTask() {
	textInput = document.getElementById("task-input");
	task = textInput.value;
	if (!(task == "")) {
		json["today"].push(task);
	}
	textInput.value = textInput.defaultValue;
	loadToday();
	if (json["today"].length) {
		loadElement("next-task", json["today"][0]);
	}
}
window.onload = function() {
		loadPage(json);
		document.documentElement.addEventListener('click', function(event) {
			if (event.target.matches("#complete-next")) {
				shiftNext(5);
			}
			if (event.target.matches("#fail-next")) {
				shiftNext(-5);
			}
			if (event.target.matches("#delete-next")) {
				shiftNext(0);
			}
			if (event.target.matches("#add-task")) {
				addTask();
			}
			if (event.target.matches(".complete-task")) {
				spliceTask(event.target.parentElement, 3);
			}
			if (event.target.matches(".fail-task")) {
				spliceTask(event.target.parentElement, -3);
			}
			if (event.target.matches(".delete-task")) {
				spliceTask(event.target.parentElement, 0);
			}
		})
}
